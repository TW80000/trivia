# Trivia Game

https://trivia.troywolters.com

## Criteria met

- Met all of the behaviour criteria for the game except users getting eliminated when they get a question wrong. I chose not to implement this behaviour because it seems unfun to get "out" and have to just sit there while other people you're playing with keep going.
- The backend is entirely using serverless technologies on AWS: API Gateway, Lambda, and DynamoDB for the backend and Amplify for serving the static frontend.
  - The services being serverless means I don't ever have to worry about security patches or maintenance of any kind except if Python 3.9 ever hits EOL.
  - You get monitoring via CloudWatch for free with these technologies as well, which is part of the reason I chose them.
  - DynamoDB has a TTL (time to live) feature built in so data is automatically deleted for me after 10 minutes meaning I don't need to worry about increasing costs over time.
  - Lambda is pay for use so I don't pay to keep servers running idle if no one's using the app.
  - Amplify gives me automatic CI/CD out of the box when I merge to the `main` git branch.

## What I would do next if I spend more time

- Get the API under the same domain as the frontend so I don't need to deal with CORS
- Prevent room codes from containing offensive strings
- Handle user names with special characters
- Parse DynamoDB JSON into normal JSON so it doesn't clutter up the codes with type attributes
- Unit & integration tests. All testing was done manually during development.
- Prevent submitting multiple answers on the server side. Right now it relies on disabling the buttons when a client submits an answer.
- Don't send the full list of questions & answers up front to the clients. Instead, only send the answers after the time period is up. This would prevent cheating by motivated and technically-savvy users.
- Replace dependency on external API for trivia questions & answers with my own question bank
- There are probably edge cases I'm not handling on the server-side
- The client side doesn't handle error responses from the server super well, could make that more user-friendly

## API

| method | resource | meaning | trigger |
| ------ | ------ | ------ | ------ |
| POST | /rooms?playerName=Foo | create a room | user action |
| GET  | /rooms/{code} | get room info | periodically called in the background on the client |
| POST | /rooms/{code}/start | start a game | host action |
| POST | /rooms/{code}/players/{name} | join a room | user action |
| POST | /rooms/{code}/players/{name}/answers/{questionIndex}/{answerIndex} | answer question | user action |