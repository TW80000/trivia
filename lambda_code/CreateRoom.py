import boto3
import json
import random
import string
from datetime import datetime, timedelta

dynamo = boto3.client('dynamodb')

def lambda_handler(event, context):
    if not "queryStringParameters" or not "playerName" in event["queryStringParameters"]:
        return respond(400, "Missing player name")
    
    playerName = event["queryStringParameters"]["playerName"]
    roomCode = findUnusedRoomCode()
    
    createRoom(roomCode, playerName)
    
    return respond(200, roomCode)

def respond(statusCode, body):
    return {
        'statusCode': statusCode,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,PUT,PATCH,GET'
        }
    }

def findUnusedRoomCode():
    while True:
        roomCode = generateRoomCode()
        if not roomExists(roomCode):
            return roomCode

def generateRoomCode():
    letters = string.ascii_uppercase
    return ''.join(random.choice(letters) for i in range(4))

def roomExists(roomCode):
    response = dynamo.get_item(
        TableName='trivia-rooms',
        Key={
            "room_id": {
                "S": roomCode
            }
        }
    )
    return "Item" in response

def createRoom(roomCode, playerName):
    dynamo.put_item(
        TableName='trivia-rooms',
        Item={
            "room_id": {
                "S": roomCode
            },
            "state": {
                "S": "LOBBY"
            },
            "host": {
                "S": playerName
            },
            "players": {
                "SS": [
                    playerName
                ]
            },
            "ttl": {
                "N": str((datetime.now() + timedelta(minutes=10)).timestamp())
            }
        }
    )