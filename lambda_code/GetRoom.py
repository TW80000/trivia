import boto3
import json

dynamo = boto3.client('dynamodb')

def lambda_handler(event, context):
    if not 'pathParameters' or not 'room' in event['pathParameters']:
        return respond(401, 'Missing room code')
    
    roomCode = event['pathParameters']['room']
    
    return respond(200, getRoomInfo(roomCode))

def respond(statusCode, body):
    return {
        'statusCode': statusCode,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,PUT,PATCH,GET'
        }
    }

def getRoomInfo(roomCode):
    response = dynamo.get_item(
        TableName='trivia-rooms',
        Key={
            'room_id': {
                'S': roomCode
            }
        }
    )
    return response['Item']