import boto3
import json

dynamo = boto3.client('dynamodb')

def lambda_handler(event, context):
    if not 'pathParameters' or not 'room' in event['pathParameters']:
        return respond(400, 'Missing room code')
    
    if not 'pathParameters' or not 'name' in event['pathParameters']:
        return respond(400, 'Missing player name')
    
    playerName = event['pathParameters']['name']
    roomCode = event['pathParameters']['room']
    response = getRoom(roomCode)
    
    if not 'Item' in response:
        return respond(404, 'Room not found')
    
    room = response['Item']
    
    if room['state']['S'] != 'LOBBY':
        return respond(400, 'That game is already in progress.')
    elif playerName in room['players']['SS']:
        return respond(400, 'That name is already taken. Try another one.')
    
    joinRoom(roomCode, playerName)
    
    return respond(200, roomCode)

def respond(statusCode, body):
    return {
        'statusCode': statusCode,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,PUT,PATCH,GET'
        }
    }

def joinRoom(roomCode, playerName):
    dynamo.update_item(
        TableName='trivia-rooms',
        Key={
            'room_id': {
                'S': roomCode
            }
        },
        UpdateExpression='ADD players :playerName',
        ConditionExpression='#state = :lobby AND NOT contains(players, :playerName)',
        ExpressionAttributeNames={
            '#state': 'state'
        },
        ExpressionAttributeValues={
            ':playerName': {
                'SS': [playerName]
            },
            ':lobby': {
                'S': 'LOBBY'
            }
        }
    )

def getRoom(roomCode):
    return dynamo.get_item(
        TableName='trivia-rooms',
        Key={
            'room_id': {
                'S': roomCode
            }
        }
    )