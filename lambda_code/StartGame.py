import boto3
import json
import random
from datetime import datetime, timedelta
from urllib.request import urlopen

dynamo = boto3.client('dynamodb')

def lambda_handler(event, context):
    if not 'pathParameters' or not 'room' in event['pathParameters']:
        return respond(400, 'Missing room code')
    
    roomCode = event['pathParameters']['room']
    response = getRoom(roomCode)
    
    if not 'Item' in response:
        return respond(404, 'Room not found')
    
    room = response['Item']
    
    if room['state']['S'] != 'LOBBY':
        return respond(400, 'That game is already in progress.')
    
    questions = loadQuestions()
    
    startGame(roomCode, questions)
    
    return respond(200, roomCode)

def respond(statusCode, body):
    return {
        'statusCode': statusCode,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,PUT,PATCH,GET'
        }
    }

def loadQuestions():
    with urlopen("https://opentdb.com/api.php?amount=8&type=multiple") as response:
        json_response = json.loads(response.read())
        return [{
            'M': {
                'question': {
                    'S': result['question']
                },
                'answers': {
                    'L': constructAnswers(result)
                }
            }
        } for result in json_response['results']]

def constructAnswers(question):
        answers = [{
            'M': {
                'answerText': { 'S': answer },
                'isCorrect': { 'BOOL': False }
            }
        } for answer in question['incorrect_answers']]
        answers.insert(random.randrange(0, 4), {
            'M': {
                'answerText': { 'S': question['correct_answer'] },
                'isCorrect': { 'BOOL': True }
            }
        })
        return answers

def startGame(roomCode, questions):
    dynamo.update_item(
        TableName='trivia-rooms',
        Key={
            'room_id': {
                'S': roomCode
            }
        },
        UpdateExpression='SET #state = :inGame, questions = :questions, startTime = :startTime',
        ConditionExpression='#state = :lobby',
        ExpressionAttributeNames={
            '#state': 'state'
        },
        ExpressionAttributeValues={
            ':inGame': {
                'S': 'IN_GAME'
            },
            ':lobby': {
                'S': 'LOBBY'
            },
            ':questions': {
                'L': questions
            },
            ':startTime': {
                'S': (datetime.now() + timedelta(seconds=5)).replace(microsecond=0).isoformat()
            }
        }
    )

def getRoom(roomCode):
    return dynamo.get_item(
        TableName='trivia-rooms',
        Key={
            'room_id': {
                'S': roomCode
            }
        }
    )