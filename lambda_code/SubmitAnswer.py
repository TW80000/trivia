import boto3
import json
from datetime import datetime, timedelta
from urllib.request import urlopen

dynamo = boto3.client('dynamodb')
ROUND_DURATION = timedelta(seconds=15)
QUESTION_DURATION = timedelta(seconds=10)

def lambda_handler(event, context):
    if not event['pathParameters'] or not 'room' in event['pathParameters']:
        return respond(400, 'Missing room code')
    
    if not event['pathParameters'] or not 'name' in event['pathParameters']:
        return respond(400, 'Missing player name')
    
    if not event['pathParameters'] or not 'questionIndex' in event['pathParameters']:
        return respond(400, 'Missing question index')
    
    if not event['pathParameters'] or not 'answerIndex' in event['pathParameters']:
        return respond(400, 'Missing answer index')
    
    roomCode = event['pathParameters']['room']
    playerName = event['pathParameters']['name']
    questionIndex = int(event['pathParameters']['questionIndex'])
    answerIndex = int(event['pathParameters']['answerIndex'])
    
    if questionIndex < 0 or questionIndex > 8:
        return respond(400, 'Invalid question')
    
    if answerIndex < 0 or answerIndex > 3:
        return respond(400, 'Invalid answer')
    
    response = getRoom(roomCode)
    
    if not 'Item' in response:
        return respond(404, 'Room not found')
    
    room = response['Item']
    
    if not playerName in room['players']['SS']:
        return respond(404, 'Player not found in room')
    
    if room['state']['S'] != 'IN_GAME':
        return respond(400, 'That game is not in progress.')
    
    startTime = datetime.fromisoformat(room['startTime']['S'])
    questionWindowStart = startTime + (ROUND_DURATION * questionIndex)
    questionWindowEnd = questionWindowStart + QUESTION_DURATION
    now = datetime.now()
    
    if (now < questionWindowStart or now > questionWindowEnd):
        return respond(400, 'Questions cannot be answered outside of the expected time range')
    
    submitAnswer(roomCode, playerName, questionIndex, answerIndex)
    
    return respond(200, 'OK')

def respond(statusCode, body):
    return {
        'statusCode': statusCode,
        'body': json.dumps(body),
        'headers': {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Headers': 'Content-Type',
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Methods': 'OPTIONS,POST,PUT,PATCH,GET'
        }
    }

def getRoom(roomCode):
    return dynamo.get_item(
        TableName='trivia-rooms',
        Key={
            'room_id': {
                'S': roomCode
            }
        }
    )

def submitAnswer(roomCode, playerName, questionIndex, answerIndex):
    dynamo.update_item(
        TableName='trivia-rooms',
        Key={
            'room_id': {
                'S': roomCode
            }
        },
        UpdateExpression='ADD questions[' + str(questionIndex) + '].answers[' + str(answerIndex) + '].chosenBy :playerName',
        ConditionExpression='#state = :inGame',
        ExpressionAttributeNames={
            '#state': 'state',
        },
        ExpressionAttributeValues={
            ':inGame': {
                'S': 'IN_GAME'
            },
            ':playerName': {
                'SS': [playerName]
            },
        }
    )